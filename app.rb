require 'sinatra'
require 'colorscore'
require 'paleta'
require 'haml'

class ColorGetty < Sinatra::Base
  include Colorscore

  get "/" do
    haml :index
  end

  post "/analyze" do
    histogram = Histogram.new("#{params[:image]}")
    scores = "<img src=\"#{params[:image]}\" /><br /><br />"
    histogram.scores.each do |score|
      color = Paleta::Color.new(:hex, score[1].hex)
      ocolor = color.complement!
      scores+= "<div style=\"width:30px; height:30px; float:left; margin-right:10px; background-color:#{score[1].html};\"></div>"
      scores+= "<div style=\"width:30px; height:30px; float:left; margin-right:10px; background-color:##{color.hex};\"></div>"
      scores+="<br /><br />"
    end
    scores
  end
end
